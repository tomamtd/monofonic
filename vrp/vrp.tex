\documentclass[a4paper,12pt]{article}
\pdfoutput=1 % if your are submitting a pdflatex (i.e. if you have
             % images in pdf, png or jpg format)

\usepackage{jcappub} 
\usepackage[T1]{fontenc} % if needed
\usepackage{amsbsy}
\usepackage{color}
\usepackage{subcaption}


\title{\boldmath Relativistic second-order initial conditions for simulations of large-scale structure}


\newcommand{\I}{\mathcal I}
\newcommand{\D}{\mathcal D}
\renewcommand{\H}{\mathcal H}
\newcommand{\K}{\mathcal K}
\newcommand{\Om}{\Omega_{m0}}
\renewcommand{\a}{a_{\mathrm{nl}}}

\begin{document}

We work in the Poisson gauge with line element:
\begin{equation}
    \label{eq:poisson}
    ds^2 = a^2 \left( -e^{2\psi}d\eta^2 + e^{-2\phi} \delta_{ij} dx^i dx^j \right)\,.
\end{equation}

\section{Back-scaling}
Let us define the first order transfer function $T^{(1)}$ as

\begin{equation}
    \label{eq:first-transfer}
    \I_1(z,\mathbf k) = T^{\I}_{1}(z,k) \zeta(\mathbf{k})\,,
\end{equation}
where $\I$ can be any field, $z$ is the redshift and $\zeta(\mathbf{k})$ is the primordial curvature perturbation eventually non-Gaussian. At first order we have $\psi_{1} = \phi_{1} = \varphi$. All quantity will be defined w.r.t. the linearely extrapolated potential $\varphi$ to today $\varphi_0$ defined as

\begin{equation}
    \label{eq:potential}
    \varphi_0(\mathbf k) = T_{\varphi}^{(1)}(\eta_0,k) \zeta(\mathbf{k})
\end{equation}
Then we can use the linear growth factor $g(\eta)=\D/a$ to back-scale the potential: $\varphi(\eta,\mathbf k) = g(\eta) \varphi_0(\mathbf k)$.

The Poisson equation reads:
\begin{equation}
    \label{eq:Poisson}
    \left(\Delta - 3 f \H^2 \right) \varphi = -\frac{3}{2} \frac{\H_0^2 \Omega_m}{a} \delta_1
\end{equation}
where $f=\dot \D / \H \D$. The density can be back-scaled with $\delta_1(\eta,\mathbf k) = \D(\eta) \delta_1(\eta_0,\mathbf k)$.


\section{The ``Villa and Rampf plus'' (VR+) kernel}
We want to find the real space expression of the ``Villa and Rampf plus'' (VR+) kernel of Ref~\href{https://arxiv.org/pdf/1602.05933.pdf}{1602.05933}. Let us define the Fourier space convolution integral 

\begin{equation}
    \label{eq:convolution}
     \int_{\mathbf k_1 \mathbf k_2}= \int \frac{d^3k_1}{(2\pi)^3}\frac{d^3k_2}{(2\pi)^3} \delta^D(\mathbf k - \mathbf k_1 -\mathbf k_2) \,.
\end{equation}
Then, the second-order density reads in Fourier space

\begin{equation}
    \label{eq:second-order-stuff}
    \frac{1}{2}\delta_2 (\eta,\mathbf k) = \left( \frac{2 \D}{3\H_0^2\Om}\right)^2  \int_{\mathbf k_1 \mathbf k_2} k_1^2 k_2^2 T^{(2)}(k_1,k_2,k) \varphi_0(\mathbf k_1) \varphi_0(\mathbf k_2)\,.
\end{equation}
where the kernel $K$ may be written like 
\begin{equation}
    \label{eq:kernel}
    T^{(2)}(k_1,k_2,k) = \beta-\alpha + \frac{\beta}{2} \mu \left(\frac{k_1}{k_2}+\frac{k_2}{k_1}\right) + \alpha \mu^2 + \gamma \left(\frac{k_1}{k_2}-\frac{k_2}{k_1}\right)^2
\end{equation}
where $\mu = \mathbf k_1 \cdot \mathbf k_2 / k_1 k_2$. 
Consider the \textbf{equilateral limit} $k_1=k_1=k$, then the last term of \eqref{eq:kernel} vanishes while, in the \textbf{squeezed limit} $k_1 \ll k_2,k$ or  $k_2 \ll k_1,k$, that last term becomes dominant. The $\gamma$ function is therefore the contribution to the squeezed limit.

The Fourier transform of eq~(5.54) of Ref~\href{https://arxiv.org/pdf/1505.04782.pdf}{1505.04782} leads to the VR kernel

\begin{equation}
    \label{eq:VR}
\begin{split}
    \alpha_{\mathrm {VR}} &= \frac{7-3v}{14} + \left( 4f +\frac{3}{2} u-\frac{9}{7}w \right)\frac{\H^2}{k^2} + \left( 18f^2+9f^2u-\frac{9}{2}fu \right)\frac{\H^4}{k^4}\\ 
    \beta_{\mathrm {VR}} &= 1 + \left( -2f + 6f-\frac{9}{2}u \right) \frac{\H^2}{k^2} + \left( 36f^2+18f^2u \right)\frac{\H^4}{k^4}\\
    \gamma_{\mathrm {VR}} &= \frac{1}{2}\left( -f^2 + f-3 u\right) \frac{\H^2}{k^2} + \frac{1}{4} \left( 18f^2+9(f^2-f)u \right)\frac{\H^4}{k^4}
\end{split}
\end{equation}
The \textbf{Newtonian limit} is recover when $\mathcal H^2 / k^2 \rightarrow 0$. This means that \textbf{relativistic corrections are important near the horison}: $k \sim \H$. Note that $\gamma_{\textrm{VR}}\rightarrow 0$ in the Newtonian limit. 

The \textbf{time dependence} is recover by multiplying each coefficient of \eqref{eq:VR} by $a^a$. In the EdS limit, $a=\eta^2$ and $\H=1/\eta$. Hence the terms $\propto \H^4 / k^4$ are \textbf{constant in time} and come from the second-order IC which vanishes in the Newtonian limit. The terms $\propto \H^2/k^2$, including the squeezed limit, grow like $a$, hence do not decay w.r.t. the linear solution. The Newtonian terms $\propto \H^0/k^0$, excluding the squeezed limit, grow like $a^2$.

The squeezed limit is, again, is recover by setting e.g. $k_1 \ll k_2 \sim k$. Thus, we ideally want to consider the modes $k_1<k_{\mathrm {eq}}i \sim 0.01$ Mpc$^{-1}$, meaning the modes that enters the horizon during the radiation domination era. In principle, one has to solve the full Einstein-Boltzmann system up to second-order, which is the purpose of SONG. 

\begin{figure}
\begin{center}
    \includegraphics[scale=0.3]{squeezed.png}
    \includegraphics[scale=0.3]{equilateral.png}
\end{center}
\label{fig:plot}
\end{figure}

Based on a separate universe approach, Ref~\href{https://arxiv.org/pdf/1602.05933.pdf}{1602.05933} found an approximation of the contribution of the early radiation to the squeezed limit. Hence, with

\begin{equation}
    \label{eq:gamma_plus}
    \gamma_{\mathrm {VR}+} =  \gamma_{\mathrm {VR}} - \frac{1}{2}\left( f + \frac{3u}{2} \right) \left(\frac{\H^2}{k^2}+3 f\frac{\H^4}{k^4}\right) \frac{\partial\log{T^{(1)}_{\phi}}}{\partial\log{k}}\,,
\end{equation}
the SONG kernel is recover with $\sim 1\%$ error. This new term has only an impact on the squeeze limit!



\section{Let see now how it translates in real space}
Let us see how to compute in real space the VR$+$ kernel. The radiation term includes a new term $\propto \partial \log T / \partial \log k$. As this is only a function of the modulus of $\mathbf k$, it can be factorized. By replacing the Dirac $\delta$ function of eq~\ref{eq:convolution} by an integral we get

\begin{equation}
    \label{eq:radiation_term}
    \begin{split}
        \frac{1}{2}\delta_2 (\mathbf k) & \supset -a^2 \frac{1}{2}\left( f + \frac{3u}{2} \right) \left(\frac{\H^2}{k^2}+3 f\frac{\H^4}{k^4}\right) \frac{\partial \log T_\phi}{\partial \log k}  \\
        &\int\frac{d^3x d^3k_1d^3k_2 }{(2\pi)^6} k_1^2k_2^2\left(\frac{k_1}{k_2}-\frac{k_2}{k_1}\right)^2 \varphi_0(\mathbf k_1) \varphi_0(\mathbf k_2) e^{i(-\mathbf k+\mathbf k_1+\mathbf k_2)\mathbf x} \,.
    \end{split}
\end{equation}
Notting that $\mathbf k = \mathbf k_1 + \mathbf k_2$, we have the idendity 
\begin{equation}
    \label{eq:identity4}
    \begin{split}
        k_1^2k_2^2 \left( \frac{k_1}{k_2} - \frac{k_2}{k_1} \right)^2  &= k^4 - 4k_1^2k_2^2\left(1+\mu \left( \frac{k_1}{k_2} + \frac{k_2}{k_1} \right)+\mu^2  \right) \\
        &= k_1^4+ k_2^4 - 2 k_1^2k_2^2 - 2 (k_1^2+k_2^2) k_1k_2\mu 
    \end{split}
\end{equation}
Therefore we get

\begin{equation}
    \label{eq:radiation_real}
    \begin{split}
        \int\frac{d^3k_1d^3k_2 }{(2\pi)^6} k_1^2k_2^2\left(\frac{k_1}{k_2}-\frac{k_2}{k_1}\right)^2 \varphi_0(\mathbf k_1) \varphi_0(\mathbf k_2) &e^{i(\mathbf k_1+\mathbf k_2)\mathbf x} =\\ 
        & 2 \varphi_0 \Delta^2 \varphi_0 - 2 (\Delta\varphi_0)^2 - 4 \varphi_0^{,i}\Delta\varphi_{0,i} \,.
    \end{split}
\end{equation}
Replacing in \eqref{eq:radiation_term}, the real-space second-order density will include the radiation term as

\begin{equation}
    \label{eq:res}
    \begin{split}
        \left(\frac{2\D^2}{3(\H_0^2\Om)^2} \right)^{-1} \frac{1}{2}\delta_2 (\mathbf x) & \supset -  \frac{1}{2}\left( f + \frac{3u}{2} \right) \int\frac{d^3k}{(2\pi)^3}\left(\frac{\H^2}{k^2}+3 f\frac{\H^4}{k^4}\right) \frac{\partial \log T_\phi}{\partial \log k}  \\
        &\int d^3x_1 \left[2 \varphi_0 \Delta^2 \varphi_0 - 2 (\Delta\varphi_0)^2 - 4 \varphi_0^{,i}\Delta\varphi_{0,i} \right]  e^{i\mathbf k (\mathbf x-\mathbf x_1)} \,.
    \end{split}
\end{equation}
Note that the integral over $x$ would give a Dirac-$\delta$ function. We don't use this simplification since we want to use FFTs. 

\section{Full real space expression}
I want here re-write eq~(5.54) of Ref~\href{https://arxiv.org/pdf/1505.04782.pdf}{1505.04782} with the function $f,u,v,w$ defined in Ref~\href{https://arxiv.org/pdf/1602.05933.pdf}{1602.05933} as

\begin{eqnarray}
    \label{eq:fuvw}
    f = \frac{\dot \D }{\D\H}\,, & \qquad &   u= \frac{\Om \H_0}{a \H^2}\,, \\
    v = \frac{7 F}{3\D^2}\,,     & \qquad &w = \frac{7 \dot F}{6 \H \D^2} \,.
\end{eqnarray}
It is also useful to use eq~(3.5) of Ref~\href{https://arxiv.org/pdf/1602.05933.pdf}{1602.05933} 
\begin{equation}
    \label{eq:3.5}
    g_{\mathrm{in}} =  \frac{\D_{\mathrm{in}}}{a_{\mathrm{in}}} = \frac{2}{5} \frac{\D \H^2}{\Om \H_0^2} \left( f+\frac{3}{2}u \right)\,.
\end{equation}
Hence, eq~(5.54) of Ref~\href{https://arxiv.org/pdf/1505.04782.pdf}{1505.04782} reads , by including the radiation term \eqref{eq:res},
\begin{equation}
    \label{eq:5.54}
    \begin{split}
        \left(\frac{2\D^2}{3(\H_0^2\Om)^2} \right)^{-1} \frac{1}{2} \delta_2 &=  \H^4\left[ \frac{9}{4}u f^2-9 uf + \frac{3}{2} (1+2\a )(f+\frac{3}{2}u) \right] \varphi_0^2 \\ 
        & - 27 \H^4 u f \Theta_{0}  + \H^2 \left[ \frac{9}{4}u- 2 \a (f+\frac{3}{2}u) \right] (\nabla \varphi_0)^2\\
        & + \H^2\frac{18}{7} w \Psi_0  + \H^2 \left[ \frac{9}{2} u+ (f+\frac{3}{2}u) (1-2\a ) + f^2\right] \varphi_0\Delta\varphi_0 \\
        & + \frac{1}{2}(1+\frac{3}{7}v)(\Delta\varphi_0)^2 +  \varphi_0^{,l} \Delta\varphi_{0,l} + \frac{1}{2} (1-\frac{3}{7}v) \varphi_0^{,lm}\varphi_{0,lm} \\
         - \frac{1}{2} \left( f + \frac{3}{2} u\right) \int\frac{d^3k}{(2\pi)^3} &\left(\frac{\H^2}{k^2}+3 f\frac{\H^4}{k^4}\right) \frac{\partial \log T^{(1)}_\phi}{\partial \log k} \\ 
        & \int d^3x_1 \left[2 \varphi_0 \Delta^2 \varphi_0 - 2 (\Delta\varphi_0)^2 - 4 \varphi_0^{,i}\Delta\varphi_{0,i} \right]  e^{i\mathbf k (\mathbf x-\mathbf x_1)} \,,  
    \end{split}
\end{equation}
where 

\begin{equation}
    \label{eq:theta}
    \begin{split}
        \Theta_0 &= \Psi_0 - \frac{1}{3} \varphi_0^{,l}\varphi_{0,l} \\
        \Psi_0 &= - \frac{1}{2}(\Delta\varphi_0)^2 +\frac{1}{2} \varphi_0^{,lm}\varphi_{0,lm} \,.
    \end{split}
\end{equation}



\end{document}
